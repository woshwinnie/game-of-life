const  unitLength  =  10;
let boxColor1  =  '#50F901';
    let colorSelector1 = document.querySelector("#boxColor1");
    colorSelector1.addEventListener("change",(e)=>{
        boxColor1=e.target.value;
    });
let boxColor2  =  '#A78301';
    let colorSelector2 = document.querySelector("#boxColor2");
        colorSelector2.addEventListener("change",(e)=>{
            boxColor2=e.target.value;
        });
let boxColor3  =  '#50B901';
    let colorSelector3 = document.querySelector("#boxColor3");
    colorSelector3.addEventListener("change",(e)=>{
        boxColor3=e.target.value;
    });
let backgroundColor  =  '#019AF9';
let backgroundColorSelector = document.querySelector("#backgroundColor");
console.log(backgroundColorSelector);
    backgroundColorSelector.addEventListener("change",(e)=>{
        backgroundColor=e.target.value;
    });
let dragColor = '#F96C01';
let dragColorSelector = document.querySelector("#dragColor");
dragColorSelector.addEventListener("change",(e)=>{
    dragColor=e.target.value;
    });


const  strokeColor  =  50;
let  columns; /* To be determined by window width*/
let  rows; /* To be determined by window height */
let  currentBoard;
let  nextBoard;

/*windowsize*/
const heightOutput = document.querySelector('#height');
const widthOutput = document.querySelector('#width');
         
         function reportWindowSize() {
           heightOutput.textContent = window.innerHeight;
           widthOutput.textContent = window.innerWidth;
         }
         
         window.onresize = reportWindowSize;
         window.addEventListener('resize', reportWindowSize);


let fr = 1;
    let frslider = document.querySelector("#myRange");
    
    console.log(frslider);
    frslider.addEventListener("change", (e)=>{
        fr=e.Target.value;
    });

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
// console.log(slider,output);
output.innerHTML = slider.value;

slider.oninput = function(e) {
  output.innerHTML = this.value;}
//   console.log(this.value);
//   fr = this.value;
// }
document.querySelector(".random-tab").addEventListener("click",function(){
    initialRandom();
})

function keyPressed(){
   if (keyCode === 80){
       loop();
   }
   if (keyCode === 79){
       noLoop();
   }
   if (keyCode === 71){
       glider();
   }
   if (keyCode === 82){
       initialRandom();
   }
   if (keyCode === 67){
    init();
    loop();
   }
}


function setup(){

    frameRate(fr);
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth,windowHeight - 100);
    console.log(windowHeight,windowWidth);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width/ unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();
    noLoop();  // Set the initial values of the currentBoard and nextBoard
}

function windowResized(){
    resizeCanvas(windowWidth,windowHeight -200);
    unitLength=(windowWidth-100)/columns;
}


/**
* Initialize/reset the board state
*/
function  init() {
    for (let  i  =  0; i  <  columns; i++) {
        for (let  j  =  0; j  <  rows; j++) {
            currentBoard[i][j] =  0;
            nextBoard[i][j] =  0;
        }
    }
}
function initialRandom(){
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            if(Math.random() > 0.8){ 
                currentBoard[i][j] = 1
            }else{
                currentBoard[i][j] = 0
            }
            nextBoard[i][j] = 0;
        }
    }
   
}



function draw() {
    frameRate(parseInt(frslider.value));
    background(backgroundColor);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1 && nextBoard[i][j] ==1){
                fill(boxColor1);  
            }else if (currentBoard[i][j] !== 1 && nextBoard[i][j] ==1){
                fill(boxColor2);
            }else if(currentBoard[i][j] == 1 && nextBoard[i][j] !==1){
                fill(boxColor3);
            }else{
                fill(backgroundColor);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

    let reproduction  =  3;
    let reproductionSelector = document.querySelector("#reproduction");
    console.log(reproductionSelector);
    reproductionSelector.addEventListener("change",(e)=>{
            reproduction=e.target.value;
    });

    let overpopulation  =  3;
    let overpopulationSelector = document.querySelector("#overpopulation");
    overpopulationSelector.addEventListener("change",(e)=>{
    overpopulation=e.target.value;
    });

    let loneliness  =  2;
    let lonelinessSelector = document.querySelector("#loneliness");
    lonelinessSelector.addEventListener("change",(e)=>{
        loneliness=e.target.value;
    });


function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i=== 0 && j ===0 ){
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < loneliness) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > overpopulation) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == reproduction) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

document.querySelector(".gosper-glider-gun").addEventListener("click",gosperGliderGun)
document.querySelector(".glider").addEventListener("click",glider)
const gosperGliderGunCode = `........................O
......................O.O
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO
OO........O...O.OO....O.O
..........O.....O.......O
...........O...O
............OO`
const gliderCode = `.O
..O
OOO`

function arrConverse(str){
    let arr = [], conversedArr = []
    let str2 = str.split("\n")
    for(let i in str2){
        arr.push(str2[i].split(""))
    }
    
    for(let i in arr){
        conversedArr[i]=[]
        for(let j in arr[i]){
            if(arr[i][j] == "."){
                conversedArr[i].push(0)
            }else if(arr[i][j] == "O"){
                conversedArr[i].push(1)
            }
        }
    }
    return conversedArr
}
function gosperGliderGun(){
    let goseperGliderGunArray = arrConverse(gosperGliderGunCode)
    for(let i = 0; i < goseperGliderGunArray.length; i++){
        for(let j = 0; j < goseperGliderGunArray[i].length ; j++){
            currentBoard[j][i] = goseperGliderGunArray[i][j]
            nextBoard[i][j] = 0;
        }
    }
}
function glider(){
    let gliderArray = arrConverse(gliderCode)
    for(let i = 0; i < gliderArray.length; i++){
        for(let j = 0; j < gliderArray[i].length ; j++){
            currentBoard[j][i] = gliderArray[i][j]
            nextBoard[i][j] = 0;
        }
    }
}



/**
 * When mouse is dragged
 */
function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if(mouseX > unitLength * columns || mouseY > unitLength * rows){
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(dragColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    
}

/**
 * When mouse is released
 */
function mouseReleased() {
    mouseDragged();
    noLoop();
}

document.querySelector('#reset-game')
    .addEventListener('click',function(){
        init();
        loop();
    });

document.querySelector('#pause')
    .addEventListener('click',function(){
        noLoop();
    });
    document.querySelector('#play')
    .addEventListener('click',function(){
        loop();
    });

/**game */
let px;
let py;
function generatePattern(px,py){
    currentBoard[px+0][py+0]=1;
    currentBoard[px+1][py+0]=1;
    currentBoard[px+0][py+1]=1;
    currentBoard[px+1][py+1]=1;
}

function removePattern(px,py){
    currentBoard[px+0][py+0]=0;
    currentBoard[px+1][py+0]=0;
    currentBoard[px+0][py+1]=0;
    currentBoard[px+1][py+1]=0;
}

window.addEventListener("keydown",checkKeyPress,false);
function checkKeyPress(){
    if(keyCode == "66"){  //B
        px=0;
        py=0;
        generatePattern(px,py);
        draw;
    } else if (keyCode == "65"){   //A
        px=(px-2+columns)%columns;
        py=py;
        generatePattern(px,py);
        draw;
    } else if (keyCode == "68"){   //D
        px=(px+2+columns)%columns;
        py=py;
        generatePattern(px,py);
        draw;
    } else if (keyCode == "87"){   //W
        px=px;
        py=(py-2+columns)%columns;
        generatePattern(px,py);
        draw;
    } else if(keyCode == "83"){ //83 = S
        removePattern(px,py);
        px = px;
        py = py + 2;
        generatePattern(px,py);
        draw();
    } else if(keyCode == "72"){ //72 = H
        px = px;
        py = py;
        glider(px,py);
        draw();
    }
        
}